'''
This will draw an image based on Pi digits
For calculating Pi used Machin's formula
http://en.literateprograms.org/Pi_with_Machin%27s_formula_%28Python%29
'''
import argparse

from PIL import Image, ImageDraw
from decimal import getcontext, Decimal

MATCHES = {'1':(255, 0,   0,   255), 
           '2':(255, 165, 0,   255), 
           '3':(255, 255, 0,   255), 
           '4':(0,   255, 0,   255), 
           '5':(176, 226, 255, 255), 
           '6':(0,   0,   255, 255), 
           '7':(238, 130, 238, 255), 
           '8':(0,   0,   0,   255), 
           '9':(255, 255, 255, 255), 
           '0':(0,   0,   0,   0  ), 
           }

def arccot(x, unity):
    '''calculate arccotangens'''
    sum = xpower = unity // x
    n = 3
    sign = -1
    while 1:
        xpower = xpower // (x*x)
        term = xpower // n
        if not term:
            break
        sum += sign * term
        sign = -sign
        n += 2
    return sum

def make_pi(digits):
    '''return pi with "digits" precision'''
    unity = 10**(digits + 10)
    pi = 4 * (4*arccot(5, unity) - arccot(239, unity))
    return pi // 10**10

def draw_pi(img_side):
    '''draws an image of Pi'''
    pi = make_pi(img_side*img_side)
    pi_str = str(pi).replace('.', '')
      
    image = Image.new("RGBA", (img_side, img_side), (0, 0, 0, 0))
    draw = ImageDraw.Draw(image)

    x = 0
    y = 0

    for i in pi_str:
        draw.point((x, y), fill=MATCHES[i])
        if x == img_side:
            x = 0
            y += 1
        else:
            x += 1

    del draw
    image.save("pi.png", "PNG")   

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('precision', type=int)
    args = parser.parse_args()
    draw_pi(args.precision)
    
main()
